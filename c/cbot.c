#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>

#include "cJSON.h"

#define PI		3.14159265359
#define TEST	0

static void switchLane(char *data);
static void turbo();
static void throttle(double throttle);
static void ping();
#if TEST == 0
static cJSON *join_msg(char *bot_name, char *bot_key);
#else
static cJSON *create_msg(char *bot_name, char *bot_key);
#endif
static cJSON *make_msg(char *type, cJSON *msg);
static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);
static void error(char *fmt, ...);
static int connect_to(char *hostname, char *port);

typedef struct Piece {
	int sw;
	int radius;

	double length;
	double angle;
} Piece;

typedef struct Car {
	char color[64];
	double length;
	double width;
	double guide;
} Car;

typedef struct Bend {
	int bendIndex;
	double *myLimit;
	double *nextLimit;
	double totalStraight;
} Bend;

int sock;
int started = 0;
char myColor[64];
int piece_length;
Piece *pieces;
int lane_length;
int *lanes;
double *totalLength;
int car_length;
Car *cars;
Bend *bends;
#if TEST == 1
FILE *ofp;
#endif
int step = 0;
int prevPieceIndex = 0;
double prevDistance = 0;
double prevVelocity = 0;
double prevAcceleration = 0;
double prevAngle = 0;
double prevAngleVelocity = 0;
int prevLane = 0;
int crashed = 0;
int turboDuration = 0;
int turboStep = 0;
double turboFactor = 1;
int totalLap = 0;
double startSpeed = 0;
double maxAngle = 0;
int startIndex = -1;

static double limitBend(double angle, double radius)
{
	return 4 + radius / 30 - angle / 180;
}

static double _abs(double value)
{
	return value>0 ? value : -value;
}

static double getRadius(int index, int lane)
{
	if(pieces[index].angle > 0)
		return pieces[index].radius - lanes[lane];
	else
		return pieces[index].radius + lanes[lane];
}

static double pieceLength(int index, int lane)
{
	if(pieces[index].length > 0)
		return pieces[index].length;
	else
		return PI * getRadius(index, lane) * _abs(pieces[index].angle) / 180;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;

    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("yourCar", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        cJSON *msg_color = cJSON_GetObjectItem(msg_data, "color");
		strcpy(myColor, msg_color->valuestring);
        puts(myColor);
    } else if (!strcmp("gameInit", msg_type_name)) {
		int i;
        msg_data = cJSON_GetObjectItem(msg, "data");
        cJSON *msg_race = cJSON_GetObjectItem(msg_data, "race");
        cJSON *msg_track = cJSON_GetObjectItem(msg_race, "track");
        cJSON *msg_pieces = cJSON_GetObjectItem(msg_track, "pieces");
        cJSON *msg_lanes = cJSON_GetObjectItem(msg_track, "lanes");
        cJSON *msg_cars = cJSON_GetObjectItem(msg_race, "cars");
		cJSON *msg_session = cJSON_GetObjectItem(msg_race, "raceSession");
		cJSON *msg_laps = cJSON_GetObjectItem(msg_session, "laps");
		if(msg_laps != 0)
			totalLap = msg_laps->valueint;
		else
			totalLap = 10;
		piece_length = cJSON_GetArraySize(msg_pieces);
		pieces = (Piece*)malloc(sizeof(Piece) * piece_length);
		for(i=0; i<piece_length; i++) {
			cJSON *msg_piece = cJSON_GetArrayItem(msg_pieces, i);
			cJSON *msg_length = cJSON_GetObjectItem(msg_piece, "length");
			cJSON *msg_switch = cJSON_GetObjectItem(msg_piece, "switch");
			cJSON *msg_radius = cJSON_GetObjectItem(msg_piece, "radius");
			cJSON *msg_angle = cJSON_GetObjectItem(msg_piece, "angle");
			if(msg_length != 0)
				pieces[i].length = msg_length->valuedouble;
			else
				pieces[i].length = 0;
			if(msg_switch != 0)
				pieces[i].sw = 1;
			else
				pieces[i].sw = 0;
			if(msg_radius != 0)
				pieces[i].radius = msg_radius->valueint;
			else
				pieces[i].radius = 0;
			if(msg_angle != 0)
				pieces[i].angle = msg_angle->valuedouble;
			else
				pieces[i].angle = 0;
			printf("piece%d %lf %d %d %lf\n", i, pieces[i].length, pieces[i].sw, pieces[i].radius, pieces[i].angle);
		}
		lane_length = cJSON_GetArraySize(msg_lanes);
		lanes = (int*)malloc(sizeof(int) * lane_length);
		totalLength = (double*)malloc(sizeof(double) * lane_length);
		for(i=0; i<lane_length; i++) {
			cJSON *msg_lane = cJSON_GetArrayItem(msg_lanes, i);
			cJSON *msg_distance = cJSON_GetObjectItem(msg_lane, "distanceFromCenter");
			cJSON *msg_index = cJSON_GetObjectItem(msg_lane, "index");
			lanes[msg_index->valueint] = msg_distance->valueint;
			printf("lane%d %d\n", msg_index->valueint, msg_distance->valueint);
		}
		car_length = cJSON_GetArraySize(msg_cars);
		cars = (Car*)malloc(sizeof(Car) * car_length);
		for(i=0; i<car_length; i++) {
			cJSON *msg_car = cJSON_GetArrayItem(msg_cars, i);
			cJSON *msg_id = cJSON_GetObjectItem(msg_car, "id");
			cJSON *msg_dimensions = cJSON_GetObjectItem(msg_car, "dimensions");
			cJSON *msg_color = cJSON_GetObjectItem(msg_id, "color");
			cJSON *msg_length = cJSON_GetObjectItem(msg_dimensions, "length");
			cJSON *msg_width = cJSON_GetObjectItem(msg_dimensions, "width");
			cJSON *msg_guideFlagPosition = cJSON_GetObjectItem(msg_dimensions, "guideFlagPosition");
			strcpy(cars[i].color, msg_color->valuestring);
			cars[i].length = msg_length->valuedouble;
			cars[i].width = msg_width->valuedouble;
			cars[i].guide = msg_guideFlagPosition->valuedouble;
			printf("car%d %s %lf %lf %lf\n", i, cars[i].color, cars[i].length, cars[i].width, cars[i].guide);
		}
		bends = (Bend*)malloc(sizeof(Bend) * piece_length * lane_length);
		int startIndex = 0;
		for(; startIndex<piece_length; startIndex++)
			if(pieces[startIndex].length <= 0)
				break;
		for(; startIndex<piece_length; startIndex++)
			if(pieces[startIndex].length > 0)
				break;
		printf("start straight index %d\n", startIndex);
		double *straightLimit = (double*)malloc(sizeof(double));
		*straightLimit = 100;
		for(i=0; i<lane_length; i++) {
			int j, prev;
			for(j=0; j<piece_length; j++) {
				int k = (j + startIndex) % piece_length;
				if(pieces[k].length > 0) {
					bends[k * lane_length + i].myLimit = straightLimit;
				} else if(pieces[prev].length > 0 || pieces[prev].angle * pieces[k].angle < 0) {
					bends[k * lane_length + i].myLimit = (double*)malloc(sizeof(double));
					double totalAngle = 0, averageRadius = 0;
					int l=k;
					while(pieces[l].length <= 0 && pieces[k].angle * pieces[l].angle > 0) {
						double a = _abs(pieces[l].angle);
						averageRadius += getRadius(l, i) * a;
						totalAngle += a;
						l = (l + 1) % piece_length;
					}
					averageRadius /= totalAngle;
					*bends[k * lane_length + i].myLimit = limitBend(totalAngle, averageRadius);
				} else {
					bends[k * lane_length + i].myLimit = bends[prev * lane_length + i].myLimit;
				}
				prev = k;
			}
			for(j=0; j<piece_length; j++) {
				int k = (j + startIndex) % piece_length;
				int l = k;
				bends[k * lane_length + i].totalStraight = 0;
				if(pieces[k].length > 0) {
					while(pieces[l].length > 0) {
						bends[k * lane_length + i].totalStraight += pieceLength(l, i);
						l = (l + 1) % piece_length;
					}
				} else {
					while(pieces[l].length <= 0 && pieces[k].angle * pieces[l].angle > 0) {
						bends[k * lane_length + i].totalStraight += pieceLength(l, i);
						l = (l + 1) % piece_length;
					}
					while(pieces[l].length > 0) {
						bends[k * lane_length + i].totalStraight += pieceLength(l, i);
						l = (l + 1) % piece_length;
					}
				}
				bends[k * lane_length + i].nextLimit = bends[l * lane_length + i].myLimit;
				bends[k * lane_length + i].bendIndex = l;
			}
			for(j=0; j<piece_length; j++) {
				printf("%d %d %d %lf %lf %lf\n", i, j, bends[j * lane_length + i].bendIndex, *bends[j * lane_length + i].myLimit, *bends[j * lane_length + i].nextLimit, bends[j * lane_length + i].totalStraight);
			}
		}
		printf("Race inited %d\n", totalLap);
    } else if (!strcmp("gameStart", msg_type_name)) {
		started = 1;
        puts("Race started");
    } else if (!strcmp("crash", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        cJSON *msg_color = cJSON_GetObjectItem(msg_data, "color");
		if (!strcmp(myColor, msg_color->valuestring)) {
			crashed = 1;
			printf("We crashed\n");
			*bends[prevPieceIndex * lane_length + prevLane].nextLimit -= 0.5;
		} else {
			printf("%s crashed\n", msg_color->valuestring);
		}
    } else if (!strcmp("spawn", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        cJSON *msg_color = cJSON_GetObjectItem(msg_data, "color");
		if (!strcmp(myColor, msg_color->valuestring)) {
			crashed = 0;
			prevPieceIndex = 0;
			prevDistance = 0;
			prevVelocity = 0;
			prevAcceleration = 0;
			prevAngle = 0;
			printf("We spawned\n");
		} else {
			printf("%s spawned\n", msg_color->valuestring);
		}
    } else if (!strcmp("turboAvailable", msg_type_name)) {
		if(crashed == 0) {
			msg_data = cJSON_GetObjectItem(msg, "data");
			cJSON *msg_duration = cJSON_GetObjectItem(msg_data, "turboDurationTicks");
			cJSON *msg_factor = cJSON_GetObjectItem(msg_data, "turboFactor");
			turboDuration = msg_duration->valueint;
			turboFactor = msg_factor->valuedouble;
			turboStep = step;
			printf("%s %d %lf\n", msg_type_name, turboDuration, turboFactor);
		} else {
			printf("%s when crashed\n", msg_type_name);
		}
    } else if (!strcmp("turboEnd", msg_type_name)) {
		turboFactor = 1;
		printf("%s\n", msg_type_name);
    } else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    } else {
		printf("%s\n", msg_type_name);
	}
	ping();
}

static int shouldlanego(int index, int lane)
{
	int i, j, result = 0;
	for(i=0; i<lane_length; i++) {
		if(i == lane)
			totalLength[i] = pieceLength(index, i);
		else
			totalLength[i] = pieceLength(index, i) * 1.2;
			
		for(j=1; j<piece_length; j++) {
			int piece = (index + j) % piece_length;
			if(pieces[piece].sw == 1) {
				break;
			} else {
				totalLength[i] += pieceLength(piece, i);
			}
		}
	}
	for(i=1; i<lane_length; i++) {
		if(totalLength[i] < totalLength[result])
			result = i;
	}
	return result;
}

static double remainLength(double startVec, double endVect)
{
	int stick = (int)(log(endVect/startVec)/log(0.98) + 0.5);
	int j;
	double length = 0;
	double vec = startVec;
	for(j=0; j<stick; j++) {
		length += vec;
		vec = vec * 0.98;
	}
	return length * 1.1;
}

static void pos_message(cJSON *msg)
{
	char *isSwitch = 0;
	int isTurbo = 0;
	double result = 1;
    cJSON *msg_data = cJSON_GetObjectItem(msg, "data");
    printf("%d ", step);
	
#if TEST == 1
	fprintf(ofp, "%d ", step);
#endif

	step++;
	int size = cJSON_GetArraySize(msg_data);
	int i;
	for(i=0; i<size; i++) {
		cJSON *msg_pos = cJSON_GetArrayItem(msg_data, i);
		cJSON *msg_id = cJSON_GetObjectItem(msg_pos, "id");
		cJSON *msg_color = cJSON_GetObjectItem(msg_id, "color");
		cJSON *msg_piecePosition = cJSON_GetObjectItem(msg_pos, "piecePosition");
		cJSON *msg_lane = cJSON_GetObjectItem(msg_piecePosition, "lane");
		double angle = cJSON_GetObjectItem(msg_pos, "angle")->valuedouble;
		int pieceIndex = cJSON_GetObjectItem(msg_piecePosition, "pieceIndex")->valueint;
		double pieceDistance = cJSON_GetObjectItem(msg_piecePosition, "inPieceDistance")->valuedouble;
		//int startLane = cJSON_GetObjectItem(msg_lane, "startLaneIndex")->valueint;
		int endLane = cJSON_GetObjectItem(msg_lane, "endLaneIndex")->valueint;
		int lap = cJSON_GetObjectItem(msg_piecePosition, "lap")->valueint;
		
		if(!strcmp(myColor, msg_color->valuestring)) {
			if(started == 0) {
				prevPieceIndex = pieceIndex;
				prevDistance = pieceDistance;
				continue;
			}
			
			if(crashed == 1)
				continue;
				
			double velocity;
			if(pieceIndex == prevPieceIndex)
				velocity = pieceDistance - prevDistance;
			else
				velocity = prevVelocity + prevAcceleration;
			double acceleration = velocity - prevVelocity;
			double angleVelocity = _abs(angle) - _abs(prevAngle);
			
			int nextPiece = (pieceIndex + 1) % piece_length;
			if(pieces[nextPiece].sw == 1 && pieceDistance + velocity * 2 > pieceLength(pieceIndex, endLane)) {
				int lane = shouldlanego(nextPiece, endLane);
				if(lane > endLane) {
					isSwitch = "Right";
				} else if(lane < endLane) {
					isSwitch = "Left";
				}
			}

			Bend bend = bends[pieceIndex * lane_length + endLane];
			double startBend = 0;
			if(pieces[pieceIndex].length != 0) {
				if(bend.bendIndex < pieceIndex && lap + 1 == totalLap) {
					startBend = *bend.myLimit;
				} else {
					startBend = *bend.nextLimit;
				}
				
				double length = remainLength(velocity, startBend);
				if(velocity < startBend || length + velocity < bend.totalStraight - pieceDistance) {
					result = 1;
					if(turboDuration != 0) {
						// length = 0;
						// double vec = velocity;
						// for(j=0; j<turboDuration; j++)
						// {
							// vec = vec * 0.98 + 0.2 * turboFactor;
							// length += vec;
						// }
						if(bend.totalStraight >= 300 && step > turboStep + 2) {
							turboDuration = 0;
							isTurbo = 1;
						}
					}
				} else {
					result = 0;
				}
			} else {
				if(pieces[prevPieceIndex].length > 0 || pieces[prevPieceIndex].angle * pieces[pieceIndex].angle < 0) {
					if(velocity < *bend.myLimit - 0.2)
						printf("Low limit ");
					else if(velocity > *bend.myLimit + 0.2)
						printf("Hight limit ");
					else
						printf("Good limit ");
				
					// if(startIndex == -1) {
						// startSpeed = velocity;
						// startIndex = pieceIndex;
						// printf("update0 %d ", startIndex);				
					// } else if(bends[startIndex * lane_length + endLane].myLimit != bend.myLimit) {
						// Bend bend1 = bends[startIndex * lane_length + endLane];
						// if(_abs(maxAngle) < 50 && startSpeed > *bend1.myLimit - 0.2) {
							// *bend1.myLimit += 0.3 * pieces[startIndex].radius / 200;
							// printf("adjust0 %d %lf ", startIndex, *bend1.myLimit);
						// } else if(_abs(maxAngle) > 55) {
							// *bend1.myLimit -= 0.3 * pieces[startIndex].radius / 200;
							// printf("adjust1 %d %lf ", startIndex, *bend1.myLimit);
						// }
						// startSpeed = velocity;
						// startIndex = pieceIndex;
						// printf("update1 %lf %d ", _abs(maxAngle), startIndex);
					// }
				}
				
				startBend = *bend.nextLimit;
				double length = remainLength(velocity, startBend);
				if(velocity < startBend || length + velocity < bend.totalStraight - pieceDistance) {
					if(velocity * 0.98 + 0.2 < *bend.myLimit) {
						result = 1;
					} else if(velocity * 0.98 + 0.2 * velocity / 8 < *bend.myLimit) {
						result = velocity / 8;
					} else {
						result = velocity / 10.5;
					}
				} else {
					result = 0;
				}
				if(_abs(angle) > 50 || _abs(angle) - _abs(prevAngle) > 3)
					result = 0;
			}
			
			if(prevAngleVelocity > 0 && angleVelocity < 0)
				maxAngle = prevAngle;
			
			printf("our car: %lf %lf %d %lf %lf ", angle, angleVelocity, pieceIndex, velocity, startBend);
			
#if TEST == 1
			fprintf(ofp, "%lf %lf %d %lf %lf ", angle, angleVelocity, pieceIndex, velocity, startBend);
#endif
			prevPieceIndex = pieceIndex;
			prevDistance = pieceDistance;
			prevVelocity = velocity;
			prevAcceleration = acceleration;
			prevAngle = angle;
			prevLane = endLane;
			prevAngleVelocity = angleVelocity;
		}
		else
		{
			//printf("enemy car: ");
		}
	}
	if(result > 1)
		result = 1;
	if(result < 0)
		result = 0;
	if(isSwitch != 0)
	{
		switchLane(isSwitch);
		printf("%s\n", isSwitch);
#if TEST == 1
		fprintf(ofp, "%s\n", isSwitch);
#endif
	}
	else if(isTurbo != 0)
	{
		turbo();
		printf("Turbo\n");
#if TEST == 1
		fprintf(ofp, "Turbo\n");
#endif
	}
	else
	{
		throttle(result);
		printf("%lf\n", result);
#if TEST == 1
		fprintf(ofp, "%lf\n", result);
#endif
	}
#if TEST == 1
	fflush(ofp);
#endif
}

int main(int argc, char *argv[])
{
    cJSON *json;

    if (argc != 5)
        error("Usage: bot host port botname botkey\n");

    sock = connect_to(argv[1], argv[2]);
#if TEST == 0
    json = join_msg(argv[3], argv[4]);
#else
    json = create_msg(argv[3], argv[4]);
#endif
    write_msg(sock, json);
    cJSON_Delete(json);

#if TEST == 1
	time_t     now;
    time(&now);
    struct tm  ts = *localtime(&now);
    char       buf[80];
	strftime(buf, sizeof(buf), "log_%Y_%m_%d_%H_%M_%S", &ts);	
	ofp = fopen(buf, "w");
#endif
    while ((json = read_msg(sock)) != NULL) {
        cJSON *msg_type;
        char *msg_type_name;

        msg_type = cJSON_GetObjectItem(json, "msgType");
        if (msg_type == NULL)
            error("missing msgType field");

        msg_type_name = msg_type->valuestring;
        if (!strcmp("carPositions", msg_type_name)) {
			pos_message(json);
        } else {
            log_message(msg_type_name, json);
        }

        cJSON_Delete(json);
    }

#if TEST == 1
	fclose(ofp);
#endif
    return 0;
}

static void switchLane(char *data)
{
	cJSON *msg = make_msg("switchLane", cJSON_CreateString(data));
	write_msg(sock, msg);
	cJSON_Delete(msg);
}

static void turbo()
{
	cJSON *msg = make_msg("turbo", cJSON_CreateString("we will rock you"));
	write_msg(sock, msg);
	cJSON_Delete(msg);
}

static void throttle(double throttle)
{
	cJSON *msg = make_msg("throttle", cJSON_CreateNumber(throttle));
	write_msg(sock, msg);
	cJSON_Delete(msg);
}

static void ping()
{
	cJSON *msg = make_msg("ping", cJSON_CreateString("ping"));
	write_msg(sock, msg);
	cJSON_Delete(msg);
}

#if TEST == 0
static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);
    return make_msg("join", data);
}
#else
static cJSON *create_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
	cJSON *createRace = cJSON_CreateObject();
    cJSON *botId = cJSON_CreateObject();
	cJSON_AddStringToObject(createRace, "msgType", "joinRace");
	cJSON_AddItemToObject(createRace, "data", data);
    cJSON_AddItemToObject(data, "botId", botId);
	cJSON_AddStringToObject(data, "trackName", "keimola");
	cJSON_AddNumberToObject(data, "carCount", 1);
    cJSON_AddStringToObject(botId, "name", bot_name);
    cJSON_AddStringToObject(botId, "key", bot_key);
	return createRace;
}
#endif

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);

    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);

    free(msg_str);
}

static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}